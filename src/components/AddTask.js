import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import {showToast} from '../common/toastify/toastify'
const API_URL = process.env.REACT_APP_API_URL;
export const ADD_TASK = `${API_URL}/taskroutes/add-task`;

const carSchema = Yup.object().shape({
  title: Yup.string(),
  description: Yup.string(),
  due_date: Yup.string(),
});

const AddTask = (props) => {
  const initialValues = {
    title: "",
    description: "",
    due_date: "",
  };
  const formik = useFormik({
    initialValues,
    validationSchema: carSchema,
    onSubmit: async (values) => {
      try {
        console.log(API_URL, values, "values");
        const response = await fetch(ADD_TASK, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            title: values.title,
            description: values.description,
            due_date: values.due_date,
          }),
        });
        const json = await response.json();
        if (!json.error) {
            showToast(json.data.message, "success");
            props.fetchTask();
            formik.resetForm();
        } else {
            showToast(json.data.message, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  return (
    <>
      {/* <button className="btn btn-success">Add Task</button> */}
      <button
        type="button"
        className="btn btn-primary"
        data-toggle="modal"
        data-target="#exampleModal"
      >
        Add Task
      </button>

      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{background: 'url("3.jpg")'}}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add Task
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div>
                <form>
                  <div className="form-group">
                    <label style={{fontWeight: "bold"}} for="exampleInputEmail1">Title</label>
                    <input
                      type="text"
                      className="form-control"
                      {...formik.getFieldProps("title")}
                      id="title"
                      name="title"
                      placeholder="Enter Title"
                    />
                    {formik.touched.title &&
                    formik.errors.title && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert" style={{ color: "red" }}>
                            {formik.errors.title}
                          </span>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="form-group">
                    <label style={{fontWeight: "bold"}} for="exampleInputEmail1">Description</label>
                    <input
                      type="text"
                      className="form-control"
                      {...formik.getFieldProps("description")}
                      id="description"
                      name="description"
                      placeholder="Enter Description"
                    />
                    {formik.touched.description &&
                    formik.errors.description && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert" style={{ color: "red" }}>
                            {formik.errors.description}
                          </span>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="form-group">
                    <label style={{fontWeight: "bold"}} for="exampleInputEmail1">Due Date</label>
                    <input
                      type="date"
                      className="form-control"
                      {...formik.getFieldProps("due_date")}
                      id="due_date"
                      name="due_date"
                    />
                    {formik.touched.due_date &&
                    formik.errors.due_date && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert" style={{ color: "red" }}>
                            {formik.errors.due_date}
                          </span>
                        </div>
                      </div>
                    )}
                  </div>
                  {/* <button type="submit" className="btn btn-primary">
                    Submit
                  </button> */}
                </form>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" onClick={formik.handleSubmit} className="btn btn-primary" data-dismiss="modal">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddTask;
