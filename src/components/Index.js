import React, { useState, useEffect } from "react";
import AddTask from "./AddTask";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_TASK = `${API_URL}/taskroutes/get-task`;
export const UPDATE_TASK = `${API_URL}/taskroutes/update-task`;
export const FILTER_TASK = `${API_URL}/taskroutes/filter-task`;

const Index = () => {

  const [all_task, setAll_task] = useState([]);
  const fetchTask = async () => {
    try {
      const response = await fetch(GET_TASK, {
        method: "GET",
      });
      const json = await response.json();
      console.log(json.data.data, "hs");
      if (!json.error) {
        setAll_task(json.data.data);
      } else {
        console.log("Not Fetched");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const filterStatus = async(e) => {
        try {
          const response = await fetch(FILTER_TASK, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              status: e.target.value,
            }),
          });
          const json = await response.json();
          if (!json.error) {
            setAll_task(json.data.data);
          } else {
            console.log("Not Fetched");
          }
        } catch (error) {
          console.error(error);
        }
  }

  const updateTask = async(id,status) => {
    console.log(id, status, "jfd")
        try {
          const response = await fetch(UPDATE_TASK, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              id: id,
              status: status,
            }),
          });
          const json = await response.json();
          console.log(json, "json")
          if (!json.error) {
            console.log("Fetched");
          } else {
            console.log("Not Fetched");
          }
        } catch (error) {
          console.error(error);
        }
  }
  useEffect(()=>{
    fetchTask();
  },[])
  return (
    <div style={{ marginLeft: "10%", marginRight: "10%", marginTop: "5%" }}>
      <div className="card" style={{ backgroundColor: "LightGray" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div className="mx-4" style={{ marginTop: "2%" }}>
            <select
                    class="form-select"
                    aria-label="Default select example"
                    onChange={filterStatus}
                    style={{height: "90%", width: "100%", borderRadius: "5px"}}
                  >
                    <option value="0">Choose Status</option>
                    <option value="Pending">Pending</option>
                    <option value="Complete" >Complete</option>
                  </select>
          </div>
          <div className="mx-4" style={{ marginTop: "2%" }}>
            <AddTask fetchTask={fetchTask}/>
          </div>
        </div>
        <div className="mx-4 my-4">
          <table className="table table-striped">
            <thead className="table-dark">
              <tr>
                <th scope="col">S No.</th>
                <th scope="col">Title</th>
                <th scope="col">Due Date</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody style={{ fontWeight: "bold" }}>
            {all_task.length===0 && <h5>No Task Found! Please Create</h5>}
              {all_task.length!==0 && all_task.map((item,key)=>
                (<>
                <tr>
                <th scope="row">{key+1}</th>
                <td>{item.title}</td>
                <td>{item.due_date}</td>
                <td>{item.description}</td>
                <td>
                  <select
                    class="form-select"
                    aria-label="Default select example"
                    onChange={(e)=>updateTask(item._id, e.target.value)}
                    style={{height: "20%", width: "50%", borderRadius: "5px"}}
                  >
                    <option value="0">{item.status}</option>
                    <option value="Pending" style={{color: "red"}}>Pending</option>
                    <option value="Complete" style={{color: "green"}}>Complete</option>
                  </select>
                </td>
              </tr>
                </>)
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Index;
