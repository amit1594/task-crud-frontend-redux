import { BrowserRouter, Routes, Route } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Navbar from "./components/Navbar";
import Index from "./components/Index";
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <ToastContainer />
      <Navbar />
      <div className="flex flex-row ...">
        <div>
        </div>
        <div className="mx-auto my-auto">
          <Routes>
            <Route path="/" element={<Index />}></Route>{" "}
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
