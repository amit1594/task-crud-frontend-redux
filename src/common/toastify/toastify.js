import { toast } from "react-toastify";

const showToast = (message, type) => {
  console.log("Yes");
  toast(message, {
    position: "top-right",
    autoClose: 3500,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    type: type,
  })
};

export { showToast };
